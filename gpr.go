
package main

import "C"

import (
	"reflect"
	"unsafe"
	"container/list"
	"fmt"
	"io"
	"os"
	"regexp"
)

var f *os.File = nil

//array for functions; key int, val pointer to a list
var functs map[int]*list.List = make(map[int]*list.List);

var gloBuf *string

//type of function pointer
type ProFunc1 func (string) *string

/*
type ProFunc interface {

	ProF(string) *string

	opCall(string a) *string {
		return ProF(a)
	}
}
*/

func BytesToString(b []byte) string {

	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}

	return *(*string)(unsafe.Pointer(&sh))
}


//export gsn
func gsn(par []byte) string{

	elt := BytesToString(par)

	return elt
}





//provides functional for access from other languages, getting a scenario as mode and its parameters as pars
//=====
//export gWrite
func gWrite(mode int, pars []string) {


///
fmt.Printf("%v\n", pars)

fmt.Println("jhgj", pars[0])
///


	//for testing purposes we define
	//aa int){
	//var pars []string
	//instead of variadic

// *  empty parameter
// %- means take output of prev func as a par


	//func performs a command list

//!!!!
fmt.Println("functs ", functs);
fmt.Println("mode ", mode);
fmt.Println("pars ", pars);

	//pointer to parameters
	val, ok := functs[mode]

fmt.Println("functs[mode] ", functs[mode]);

fmt.Println("val ", val);
fmt.Println("ok ", ok);

	if ok {
		//number of modes
		sz := (*val).Len()
		
fmt.Println("sz, num of modes ", sz);

fmt.Println("*val ", *val, " val ", val);

		//number of parameters
		psz := len(pars)

fmt.Println("pzs, num of parameters ", psz);

///
fmt.Println(sz, "|||", psz)
///
		//parameters zero
		if psz < sz {
///
fmt.Println("add")
///
			h := sz - psz

			for i := 0; i < h; i++ {
				pars = append(pars, "")
			}
		}

		var prev string = ""
		var yee *string

		i := 0

		//iterate one by one on functions for the selected command
		for v := val.Front(); v != nil; v = v.Next() {

			
			pf := v.Value.(ProFunc1)
///
fmt.Println("value of pf ", pf);
///
			//and get respected par
			if pars[i] == "%-" {
///
fmt.Println("a")
///
				yee = pf(prev)

			} else {
				///
				fmt.Println("wr |", pars[i], " ")
				///
				//Perform cur operation
				yee = pf(pars[i]);

				///
				fmt.Println("w ", yee, " ", reflect.TypeOf(yee));
				///
			}

			if yee != nil {

				prev = string(*yee)

			} else {

				prev = ""
			}


			i++
		}
	}
}

//functions that do components of a task

//=====
func OpnF(nm string) *string{

        var err error

///
fmt.Println("o", nm)
///

	f, err = os.Create(nm)

	if err != nil {

		fmt.Println(err)
	}

	return nil
}

//=====
func ReadF(re string) *string{

//Just reading a previous line at the file, applying a mask to it, providing a result

	var cursor int64 = 0

fmt.Println("rr1");

	stat, _ := f.Stat()
fmt.Println("stat ", stat);

	filesize := stat.Size()

fmt.Println("filesize ", filesize);

	cha := make([]byte, 1)

fmt.Println("r 1");

	for {
		cursor -= 1

fmt.Println("r 2");

		_, err := f.Seek(cursor, io.SeekEnd)
fmt.Println("r 3");
		if err != nil {

			fmt.Println(err)
		}
fmt.Println("r 4");
		f.Read(cha)
fmt.Println("r 5");
fmt.Println("cursor ", cursor, " cha[0] ", cha[0]);
		if cursor != -1 && (cha[0] == 10 || cha[0] == 13) { // stop if we find a line
			break
		}
fmt.Println("r 6");
		if cursor == -filesize { // stop if we are at the begining
			break
		}
fmt.Println("r 7");
	}
fmt.Println("r 8");
	//pos := filesize + cursor

	newdat := make([]byte, -1*cursor)
fmt.Println("r 9");
	f.Read(newdat)

	//apply regexpr
	exp, qwq := regexp.Compile(re)
	
	if( qwq != nil ) {
		fmt.Println(qwq);
	}

fmt.Println("r 10 newdat ", newdat, " exp ", exp, " re ", re);

	var retv = (string(exp.Find(newdat)))

fmt.Println("r 11");

	return &retv;
}

//=====
func WriteF(Str string) *string{

	///
	fmt.Println("wf")
	///

	_, err := f.Seek(0, io.SeekEnd)

	if err != nil {

		fmt.Println(err)
	}

	fmt.Fprintln(f, Str)

	return nil
}

//=====
func CloseF(str string) *string{

///
fmt.Println("c")
///

        if f != nil {

		err := f.Close()

	   if err != nil {

		fmt.Println(err)
	   }

        } else {
		fmt.Println("absent File ...")
	}

	return nil
}

//for multi buffer: on r and wr as a parameter provide a number of buf; number of fuffers is sent as a parameter on application startUp
//=====
func WriteB(Str string) *string{

	//filling buffer with the data
	gloBuf = &Str;
	//and giving these data also
	return &Str;
}

//=====
func ReadE_B(Str string) *string{

	//cleaning buf
	var buf = gloBuf;
	gloBuf = nil;

	//answering with the info
	return buf;
}


//=====
func ReadB(Str string) *string{

	//answering with the info
	return gloBuf;
}

//it sets up remote flows

//export SetMode
func SetMode (Strm string) int {

	//getting the current cursor
	y := len(functs);

	//acquiring the list of functions
	sd := parse(Strm);

	//if right
	if sd != nil {
		//put a new scenario to the functs array as a list of pointers
		functs[y] = sd;

	} else {

		fmt.Println("th operation of job ", y, "th, skipping");
	}

	return y;
}

// analyzes the text flows

func parse(Strm string) *list.List {

	///
	fmt.Println(Strm)
	///

	//create a new list
	a := list.New()
///
fmt.Println("f pointer ", ProFunc1(OpnF));
///

///
fmt.Println("a fre ", a);
///	//depending on the value of current step
	for ind, el := range Strm {

		//adding new pointer
		if el == '1' {
			a.PushBack(ProFunc1(OpnF))
		} else if el == '2' {
			a.PushBack(ProFunc1(ReadF))
		} else if el == '3' {
			a.PushBack(ProFunc1(WriteF))
		} else if el == '4' {
			a.PushBack(ProFunc1(CloseF))
		} else if el == '5' {
			//write buf
			a.PushBack(ProFunc1(WriteB))	
		} else if el == '6' {
			//read erase buf
			a.PushBack(ProFunc1(ReadE_B))
		} else if el == '7' {		
			//read buf
			a.PushBack(ProFunc1(ReadB))
		} else {
			//in care one error in scenario where not supported operation is set we do not process the whole
			fmt.Print("Parse error pos ", ind)
			return nil
		}

///
fmt.Println("a cur ", a);
///

///
//fmt.Println("a.list ",(a).root.list,"\na.prev ", *(a).(root).prev, "\na.next ", *(a).(root).next, "\na.Value ", *(a).(root).Value );
///
	}



	return a
}

//for the export purpose

//=====
func main() {}
