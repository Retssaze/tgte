//DMD64 D Compiler 2.072.2

//\n(\b([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\b)\n{2}

import std.stdio;
import std.string;
import core.stdc.string;
import core.vararg;
import std.typecons;
import std.conv : to;
import std.random;
import core.thread;
import core.atomic;
import std.parallelism;

extern (C) int SetMode(GoString);
extern (C) void gWrite(int, GoSliceS);
/*
int SetMode(GoString)
{
	return 1;
}

void gWrite(int, GoSliceS)
{

}
*/

struct GoString
{
	const char* p;
	ulong n;
};
struct GoSliceS
{
	GoString* data;
	long len;
	long cap;
};

//these commands
GoString c1 = {"1", 1},
		 c2 = {"3", 1},
// read wrb clo open reb wr
// 2 5 4 1 6 3
		 c3 = {"2541633", 7},
		 c4 = {"4", 1};

///
//extern (C) extern GoString;
//extern (C) extern GoSlice;
///



const string FNM = "tsf";
const string FTN = ".txt";

shared int MODEOPN, MODEWRT, MODECG, MODECL;

const int RPCNT = 100;

shared(int) RPCU = 0;

shared  /*immutable*/ Access acc = new shared Access();

/*shared*/
ApTsk ts1, ts2;
shared FileN FN;

//Important ---> Initializing the tasks!!!
static this()
{
	FN = new shared FileN(FNM, FTN);

	ts1 = new  /*shared*/ ApTsk("A", &FN);
	ts2 = new  /*shared*/ ApTsk("B", &FN);
}


GoString ggc = GoString("", 0);

//here we have a test functional in DLang
//the import is from Golang code

synchronized  /*static immutable*/
class Access
{
	private int ssa;

	//wrapping here
	public void dWrapC(int mode, GoSliceS par = GoSliceS(&ggc, 0, 0))
	{

		///
		writeln("Access dWrapC");
		///

		gWrite(mode, par);

		///
		writeln("/Access dWrapC");
		///

	}

	public void opCall(int mode, GoSliceS par = GoSliceS(&ggc, 0, 0))
	{
		///
		writeln("Access opCall");
		///

		dWrapC(mode, par);

		///
		writeln("/ Access opCall");
		///
	}

	void call()
	{
		ssa = 5;
		///
		writeln("Access call");
		///

	}
}

synchronized class Counter
{
	private int val;
	private int max;

	this(int c = 10)
	{
		val = -1;
		max = c;
	}

	public int Get()
	{
		return val = (val + 1) % (max + 1);
	}

	public int opCall()
	{
		return Get();
	}

	bool Check()
	{
		///
		writeln("Counter check");
		///
		return !val;
	}
}

synchronized class Msg
{
	private Counter cn;
	private string tPrt;

	this(string prt = "")
	{
		cn = new shared Counter();
		///
		writeln("Msg constructor");
		///
		tPrt = prt;
	}

	string opCall()
	{
		///
		writeln("Msg");
		///

		string r = tPrt ~ to!string(cn());

		///
		writeln("/Msg");
		///

		return r;
	}

	bool Check()
	{
		///
		writeln("Msg check");
		///

		bool a;

		a = cn.Check();

		///
		writeln("/Msg check");
		///

		return a; //cn.Check();
	}
}

synchronized class FileN
{
private:
	string nm;
	int ind = 0;
	string ex;

public:
	this(string a = "", string b = "")
	{
		nm = a;
		ex = b;
	}

	string opCall()
	{
		core.atomic.atomicOp!"+="(this.ind, 1);

		return nm ~ to!string(ind) ~ ex;
	}
}

/*synchronized*/
class ApTsk
{
	private shared Msg ms;
	private shared FileN* nm;

	this(string meg, shared FileN* nam)
	{
		///
		writeln("tkconsR");
		///

		ms = new shared Msg(meg);
		///
		nm = nam;
	}

	void opCall()
	{
		///
		writeln("ApTsk");
		///
		auto rnd = Random(unpredictableSeed);
		int b;

		//acc(MODEOPN,GetSl(GetSt("aaa.txt")));
		acc(MODEWRT, GetSl(GetSt(ms())));

		while (RPCU < RPCNT)
		{

			///
			writeln("ApTsk cycle");
			///

			int wewew = uniform!"[]"(1, 100, rnd);
			b = wewew;
			///
			writeln("5533ss");
			///
			Thread.sleep(dur!("msecs")(b));
			///
			writeln("44");
			///

			writeln(ms());

			if (ms.Check())

			{
				///
				writeln("ApTsk chck");
				///
				//acc(MODECG, "*", (*nm)(), null, ms());

// read*; wrb %-; cl nil, op nm; reb nil, wr %-, wr ms

				acc(MODECG, GetSl(GetSt("[A-Za-z0-9_]*"), GetSt("%-"), GetSt(""), GetSt((*nm)()), GetSt(""), GetSt("%-"), GetSt(ms())));

				RPCU.atomicOp!"+="(1);

				///
				writeln("/ApTsk chck");
				///
			}
			else
			{
				acc(MODEWRT, GetSl(GetSt(ms())));
				//RPCU.atomicOp!"+="(1);
			}

			///
			writeln("/ApTsk cycle");
			///

		}
		///
		writeln("/ApTsk");
		///

	}
}

/*
s.ptr
s.length


alias string=immutable(char)[]

string ss
ss~="xxx"

int[] xx;
x~=[2,3,4,];

x.ptr
.x.length
x.sizeof
x.init

int x=10;

x.init

*/
GoString GetSt(string s)
{
	///
	writeln("GetSt");
	///

	auto arr = toStringz(s);

	//GoString sgz = GoString(arr, strlen(arr));

	//GoString sgz = GoString("Mse" /*s.ptr*/ , 3 /*s.length*/ );
	GoString sgz = GoString(s.ptr, s.length);
	///
	writeln("/GetSt");
	///

	return sgz;
}

GoSliceS GetSl(GoString[] par...)
{
	///
	writeln("GetSl");
	///

	GoSliceS hgf = GoSliceS(par.ptr, par.length, par.length);

	///
	writeln("/GetSl");
	///

	return hgf;
}

void main(string[] args)
{
	writeln("Hi!!! !!!!!");
	//     Counter cnt1, cnt2;

	MODEOPN = SetMode(c1);
	MODEWRT = SetMode(c2);
	MODECG = SetMode(c3);
	MODECL = SetMode(c4);

	///
	//FileN nfn1 = new FileN("a","b");
	///

	//Access uj;
	//     uj.call();

	//     acc.call();
	///

	acc(MODEOPN, GetSl(GetSt(FN())));



	auto tks = [ts1, ts2];

	//	args = args[1 .. $];

	if (args && args.length && args[0] && args[0] == "var") //first method
	{
		foreach (ts; parallel(tks))
		{
			ts();
		}
	}
	//second method
	else
	{
		auto tts = task!ts1();
		auto tts_2 = task!ts2();

		//creating two threads
		TaskPool tptpt = new TaskPool(2);

		//add tasks
		tptpt.put(tts);
		tptpt.put(tts_2);

		//  ts1();
		//tts.executeInNewThread();
		//       tts.yieldForce;

		tptpt.finish(true);

	}
	acc(MODECL);

	int i = 0, ww = 100;

	while (i < ww)
	{
		writeln("step" ~ to!string(i));

		i++;
	}
}
