//import core.stdc.string;
//import std.string;
import core.atomic;
import std.conv : to;
import std.stdio;
import std.parallelism;


class ApTsk
{
	private string _str;
	private int _cn;
	private int _mmc;

	this(string s, int mc)
	{
		_mmc = (mc && mc>0) ? mc : 0;
		_cn = 0;
		_str = s;
	}

	void opCall()
	{
		while(_cn <_mmc)
		{
			writeln(_str ~to!string(_cn));	
			_cn ++;
		}
	}
}

ApTsk ts1, ts2;

static this()
{
	ts1 = new  ApTsk("Mess 1, ", 100);
	ts2 = new  ApTsk("Mess 2, ", 100);
}

void main(string[] args)
{

writeln(args);


	auto tks = [ts1, ts2];

	//	args = args[1 .. $];

	if (args && args.length == 2 && args[1] && args[1] == "var") //first method
	{
		foreach (ts; parallel(tks))
		{
			writeln("aa");
			ts();
		}
	}
	//second method
	else
	{
		auto tts = scopedTask!ts1();
		auto tts_2 = scopedTask!ts2();

		//creating two threads
		TaskPool tptpt = new TaskPool(2);

		//add tasks
//		tptpt.put(tts);
		tptpt.put(tts_2);
writeln("egg 0" );
		//tts.executeInNewThread();
		ts1();
		tts_2.yieldForce;

		tptpt.finish(true);

	}


	int i = 0, ww = 100;

	while (i < ww)
	{
		writeln("step" ~ to!string(i));

		i++;
	}
}
