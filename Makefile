define colorecho
      @tput setaf 3
      @echo $1
      @tput sgr0
endef



.DEFAULT_GOAL := main
.PHONY: clean
unexport LD_LIBRARY_PATH
.SHELL: export LD_LIBRARY_PATH := $(PWD)

export LD_LIBRARY_PATH := $(PWD)


$(eval export LD_LIBRARY_PATH := $(PWD))
$(shell export LD_LIBRARY_PATH)



main:	g c d

g:
	$(call colorecho,"   compiling G code ...")
	go build -o gpr.so -buildmode=c-shared gpr.go
	@#go tool cgo build -o gpr.so  gpr.go
	$(call colorecho, "   g code read ok")

c:
	$(call colorecho, "   compiling c code ...")
	gcc -c cpr.c -o cpr.o
	$(call colorecho, "   c code read ok")

d:
	$(call colorecho, "   compiling d code ...")
	$(call colorecho, "    1 ...")

	dmd -main -release dpr1.d cpr.o gpr.so
	
	$(call colorecho, "    nice\n    2 ...")

	dmd -debug dpr2.d gpr.so
	$(call colorecho, "    nice")
	$(call colorecho, "   d code read ok")

clear:
	$(call colorecho, "   sweeping the dir ...")
	$(call colorecho, "    .")
	@rm -f *.o
	$(call colorecho, "    .")
	@rm -f *.so
	$(call colorecho, "    .")
	@rm -f *.a
	$(call colorecho, "    .")
	@rm -f *.out
	$(call colorecho, "    .")
	@find . -type f -executable -print -o -name ". !" -name "ins" -o -prune | xargs rm -f
	$(call colorecho, "   cool")
